#!/bin/bash

MYNAME="Aure"

echo "¡Hello $USER!"

echo -e "\nMe llamo $MYNAME, encantado!"

echo "Hora y Fecha: $(date)"

#Para ver usuarios conectados, tenemos varios comandos, ejemplo: who,w,users...
#Yo utilizaré who

echo -e "\nUsuarios conectados al Sistema:\n$(who)"
